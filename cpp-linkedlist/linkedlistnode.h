#pragma once
/*simple struct for a linked list node
	int data;
	linkedlistnode* child
*/
struct linkedlistnode{
	int data;
	//pointer to child node
	linkedlistnode* child;
};