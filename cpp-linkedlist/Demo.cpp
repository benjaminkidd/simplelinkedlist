#include "stdafx.h"
#include "Demo.h"

#include <iostream>

Demo::Demo() {
	linkedlistnode* start = createLinkedList(5);
	printList(start);
}

Demo::~Demo(){
}

//prints demo information
void Demo::info() {
	std::cout << "Author: Benjamin Kidd  \nContent: A simple implementation of a linked list";
	std::cout << "\nNode Structure: \n struct linkedlistnode{ \n int data; \n linkedlistnode* child; \n };";
}
//returns start node of LinkedList
linkedlistnode* Demo::createLinkedList(int size) {
	linkedlistnode* nodeA = new linkedlistnode();
	nodeA->data = 0;
	for (int i = 1; i < size; i++) {
		addNodeToEnd(*nodeA, i);
	}
	return nodeA;
}

//cycles through the linked list adding a new node to the end with the data provided
void Demo::addNodeToEnd(linkedlistnode& node,int data){
	if (node.child != nullptr)addNodeToEnd(*node.child, data);
	else {
		node.child = new linkedlistnode{data,nullptr};
	}
}

//returns true if node found, returns false if node not found
bool Demo::removeNode(linkedlistnode* node,int data){
	/*
		first node is the node that needs to be removed
		delete current node and then set current node to the child node
	*/
	if (node->data == data) { 
		linkedlistnode* child = node->child;
		delete node;
		node = child;
		return true;
	}
	//while node.child != data || nullptr keep going 
	while (node->child->data != data || node->child != nullptr) {
		return removeNode(node->child, data);
	}
	//The child node is the one that needs to be removed
	//delete child node, set current nodes child to childs child
	if (node->child->data == data) {
		linkedlistnode* cchild = node->child->child;
		delete node->child;
		node->child = cchild;
	}
}

void Demo::printList(linkedlistnode *a) {
	if (a->child == nullptr) {
		std::cout << "\n Node:" << a->data << " child: nullptr";
		return;
	}
		std::cout << "\n Node:" << a->data << " child:" << a->child->data;
		printList(a->child);
	
	
}
