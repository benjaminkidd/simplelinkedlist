#pragma once
#include "linkedlistnode.h"
//Demo class for my simple linkedlist
 class Demo
{
public:
	Demo();
	~Demo();
	//print demo info
	static void info();
	//created a linked list of size (int size) and returns a pointer to the first element
	linkedlistnode* createLinkedList(int size);

	void addNodeToEnd(linkedlistnode& start,int data);

	bool removeNode(linkedlistnode * node, int data);

	void printList(linkedlistnode * a);
	

	
};

